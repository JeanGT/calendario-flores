<?php
include_once $_SERVER['DOCUMENT_ROOT'] . "/src/helper/trata_string.php";

class Flor {
	private $nomesDosMeses = ["janeiro", "fevereiro", "marco", "abril", "maio", "junho", "julho", "agosto", "setembro", "outubro", "novembro", "dezembro"];
		
    private $conn;
	
    private $nome;
    private $especie;
	private $descricao;
	private $mesesFloresceBool; //array de 12 booleans, cada um representando se a flor floresce no mês respectivo
	private $abelhasQuePolinizam;
  
    public function __construct($db){
        $this->conn = $db;	
    }
	
	function create(){
		$erroAoCadastrar = FALSE;
		$this->conn->autocommit(FALSE);
		
		//cadastro da flor
		$query = "INSERT INTO FLOR 
					(NOME, ESPECIE, DESCRICAO, JANEIRO, FEVEREIRO, MARCO, ABRIL, MAIO, JUNHO, JULHO, AGOSTO, SETEMBRO, OUTUBRO, NOVEMBRO, DEZEMBRO) 
				VALUES 
					(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param("sssiiiiiiiiiiii", $this->nome, $this->especie, $this->descricao, $this->mesesFloresceBool[0], 
				$this->mesesFloresceBool[1], $this->mesesFloresceBool[2], $this->mesesFloresceBool[3], $this->mesesFloresceBool[4],
				$this->mesesFloresceBool[5], $this->mesesFloresceBool[6], $this->mesesFloresceBool[7], $this->mesesFloresceBool[8],
				$this->mesesFloresceBool[9], $this->mesesFloresceBool[10], $this->mesesFloresceBool[11]);

		if(!$stmt->execute()){
			$erroAoCadastrar = TRUE;
		}
		
		//cadastro das abelhas que polinizam a flor
		if($this->abelhasQuePolinizam != ""){
			$query = "INSERT INTO ABELHA_POLINIZA_FLOR
						(NOME_ABELHA, NOME_FLOR) 
					VALUES 
						(?,?);";
						
			$stmt = $this->conn->prepare($query);
			$stmt->bind_param("ss", $abelha, $this->nome);

			foreach ($this->abelhasQuePolinizam as $abelha){
				if(!$stmt->execute()){
					$erroAoCadastrar = TRUE;
				}
			}
		}
		
		if($erroAoCadastrar){
			$this->conn->rollback();
		} else {
			$this->conn->commit();
		}
		
		return !$erroAoCadastrar;
	}
	
	//retorna todas as flores, filtrando opcionalmente por meses
	function read_sem_abelha($mesesFiltrados){
		$query = "SELECT 
					NOME, ESPECIE, DESCRICAO 
				FROM 
					FLOR ";
		
		if($mesesFiltrados != []){
			$query .= "WHERE ";
			
			foreach($mesesFiltrados as $mesFiltrado){
				foreach($this->nomesDosMeses as $mes){
					
					if($mesFiltrado == $mes){
						if($mesFiltrado === reset($mesesFiltrados)){ //Confere se é a primeira iteração do laço
							$query .= $mes."=1 ";
						} else {
							$query .= "AND ".$mes."=1 ";
						}
					}
					
				}
			}
		}

		$stmt = $this->conn->prepare($query);
        $stmt->execute();
  
        return $stmt->get_result()->fetch_all();
	}
	
	//retorna flores filtrando por abelhas e opcionalmente por meses também
	function read_com_abelha($mesesFiltrados, $abelhasFiltradas){
		$query = "SELECT
					NOME, ESPECIE, DESCRICAO 
				FROM 
					FLOR AS F, ABELHA_POLINIZA_FLOR AS P 
				WHERE 
					P.NOME_FLOR = F.NOME AND P.NOME_ABELHA = ? ";

		foreach($mesesFiltrados as $mesFiltrado){
			foreach($this->nomesDosMeses as $mes){
				
				if($mesFiltrado == $mes){
					$query .= "AND ".$mes."=1 ";
				}
				
			}
		}

		$stmt = $this->conn->prepare($query);
		$stmt->bind_param("s", $abelhasFiltradas);
		$stmt->execute();
  
        return $stmt->get_result()->fetch_all();
	}
	
	function setNome($nome){
		$this->nome = TrataString::tratarString($nome);
	}
	
	function setEspecie($especie){
		$this->especie = TrataString::tratarString($especie);
	}
	
	function setDescricao($descricao){
		$this->descricao = TrataString::tratarString($descricao);
	}
	
	function setMesesFloresceBool($mesesFloresce){
		//converte o array de string de meses em um array booleano
		$this->mesesFloresceBool = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
		foreach($mesesFloresce as $mesFloresce){
			for($i = 0; $i < 12; $i++){
				if($mesFloresce == $this->nomesDosMeses[$i]){
					$this->mesesFloresceBool[$i] = 1;
				}
			}
		}
	}
	
	function setAbelhasQuePolinizam($abelhasQuePolinizam){
		//converte a string "abelhasQuePolinizam" em um array de strings
		$this->abelhasQuePolinizam = explode(",", $abelhasQuePolinizam);
		foreach($this->abelhasQuePolinizam as $key => $value){
			$this->abelhasQuePolinizam[$key] = TrataString::tratarString($value);
		}
	}
}
?>