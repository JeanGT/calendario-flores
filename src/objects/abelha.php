<?php
include_once $_SERVER['DOCUMENT_ROOT'] . "/src/helper/trata_string.php";

class Abelha {
    private $conn;
	
    private $nome;
    private $especie;
  
    public function __construct($conn){
        $this->conn = $conn;
    }
	
	function create(){
		$query = "INSERT INTO ABELHA 
					(NOME, ESPECIE) 
				VALUES 
					(?,?);";
		
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param("ss", $this->nome, $this->especie);

		return $stmt->execute();
	}
	
	function setNome($nome){
		$this->nome = TrataString::tratarString($nome);
	}
	
	function setEspecie($especie){
		$this->especie = TrataString::tratarString($especie);
	}
  
}
?>