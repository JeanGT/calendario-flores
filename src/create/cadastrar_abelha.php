<?php
include_once $_SERVER['DOCUMENT_ROOT'] . "/src/config/database.php";
include_once $_SERVER['DOCUMENT_ROOT'] . "/src/objects/abelha.php";

$database = new Database();
$conn = $database->getConnection();

$abelha = new Abelha($conn);
$abelha->setNome($_GET["nomeAbelha"]);
$abelha->setEspecie($_GET["especieAbelha"]);

if ($abelha->create()) {
	echo "Abelha cadastrada com sucesso!";
} else {
	echo "Ocorreu um erro ao cadastrar a abelha.";
}

?>