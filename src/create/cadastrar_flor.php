<?php
include_once $_SERVER['DOCUMENT_ROOT'] . "/src/config/database.php";
include_once $_SERVER['DOCUMENT_ROOT'] . "/src/objects/flor.php";

$database = new Database();
$conn = $database->getConnection();
 
$meses = [];
if(isset($_GET["meses"])){
	$meses = $_GET["meses"];
}

$flor = new Flor($conn);
$flor->setNome($_GET["nomeFlor"]);
$flor->setEspecie($_GET["especieFlor"]);
$flor->setDescricao($_GET["descricaoFlor"]);
$flor->setMesesFloresceBool($meses);
$flor->setAbelhasQuePolinizam($_GET["abelhasQuePolinizam"]);

if ($flor->create()) {
	echo "Flor cadastrada com sucesso!";
} else {
	echo "Ocorreu um erro ao cadastrar a flor.";
}
?>