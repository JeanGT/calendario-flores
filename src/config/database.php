<?php
class Database{
    private $host = "localhost";
    private $db_name = "calendarioFlores";
    private $username = "root";
    private $password = "";
    private $conn;

    public function getConnection(){ 
        $this->conn = null;
   
        try{
            $this->conn = new mysqli($this->host, $this->username, $this->password, $this->db_name);
        } catch(mysqli_sql_exception $exception) {
            echo "Connection error: " . $exception->getMessage();
        }
		
		$this->conn->set_charset("utf8");
   
        return $this->conn;
    }
}
?>