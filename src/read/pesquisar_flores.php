<?php
include_once $_SERVER['DOCUMENT_ROOT'] . "/src/config/database.php";
include_once $_SERVER['DOCUMENT_ROOT'] . "/src/objects/flor.php";

$database = new Database();
$conn = $database->getConnection();

$flor = new Flor($conn);

$filtrouPorMeses = isset($_GET["meses"]);

$filtrouPorAbelhas = FALSE;
if(isset($_GET["nomeAbelhas"])){
	if($_GET["nomeAbelhas"] != ""){
		$filtrouPorAbelhas = TRUE;
	}
}

//encaminha a pesquisa para a função apropriada
if($filtrouPorAbelhas){
	if($filtrouPorMeses){
		$result = $flor->read_com_abelha($_GET["meses"], $_GET["nomeAbelhas"]);
	} else {
		$result = $flor->read_com_abelha([], $_GET["nomeAbelhas"]);
	}
} else {
	if($filtrouPorMeses){
		$result = $flor->read_sem_abelha($_GET["meses"]);
	} else {
		$result = $flor->read_sem_abelha([]);
	}
}

//Exibe o $result em uma tabela
echo "<h1> Flores: </h1> 
<table style='width:50%'> 
<tr>
	<th> Nome </th>
	<th> Espécie </th>
	<th> Descrição </th>";

foreach($result as $row){
	echo "</tr> <tr>";
	foreach($row as $value){
		echo "<th>".$value."</th>";
	}
}
echo "</tr> </table>";
?>