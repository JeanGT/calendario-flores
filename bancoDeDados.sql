-- MySQL dump 10.13  Distrib 8.0.21, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: calendarioflores
-- ------------------------------------------------------
-- Server version	5.7.31

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `abelha`
--

DROP TABLE IF EXISTS `abelha`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `abelha` (
  `nome` varchar(255) NOT NULL,
  `especie` varchar(255) NOT NULL,
  PRIMARY KEY (`nome`,`especie`),
  UNIQUE KEY `nome` (`nome`),
  UNIQUE KEY `especie` (`especie`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `abelha`
--

LOCK TABLES `abelha` WRITE;
/*!40000 ALTER TABLE `abelha` DISABLE KEYS */;
INSERT INTO `abelha` VALUES ('guarupu','melipona bicolor'),('iraí','nannotrigona testaceicornes'),('uruçu','melipona scutellaris'),('uruçu-amarela','melipona rufiventris');
/*!40000 ALTER TABLE `abelha` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `abelha_poliniza_flor`
--

DROP TABLE IF EXISTS `abelha_poliniza_flor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `abelha_poliniza_flor` (
  `nome_abelha` varchar(255) NOT NULL,
  `nome_flor` varchar(255) NOT NULL,
  PRIMARY KEY (`nome_flor`,`nome_abelha`),
  KEY `nome_abelha` (`nome_abelha`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `abelha_poliniza_flor`
--

LOCK TABLES `abelha_poliniza_flor` WRITE;
/*!40000 ALTER TABLE `abelha_poliniza_flor` DISABLE KEYS */;
INSERT INTO `abelha_poliniza_flor` VALUES ('guarupu','girassol'),('guarupu','orquídea'),('iraí','copo-de-leite'),('iraí','girassol'),('iraí','orquídea'),('iraí','tulipa'),('uruçu','girassol'),('uruçu','rosa'),('uruçu-amarela','copo-de-leite'),('uruçu-amarela','lírio'),('uruçu-amarela','tulipa');
/*!40000 ALTER TABLE `abelha_poliniza_flor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `flor`
--

DROP TABLE IF EXISTS `flor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `flor` (
  `nome` varchar(255) NOT NULL,
  `especie` varchar(255) DEFAULT NULL,
  `descricao` varchar(1000) DEFAULT NULL,
  `janeiro` tinyint(1) NOT NULL,
  `fevereiro` tinyint(1) NOT NULL,
  `marco` tinyint(1) NOT NULL,
  `abril` tinyint(1) NOT NULL,
  `maio` tinyint(1) NOT NULL,
  `junho` tinyint(1) NOT NULL,
  `julho` tinyint(1) NOT NULL,
  `agosto` tinyint(1) NOT NULL,
  `setembro` tinyint(1) NOT NULL,
  `outubro` tinyint(1) NOT NULL,
  `novembro` tinyint(1) NOT NULL,
  `dezembro` tinyint(1) NOT NULL,
  PRIMARY KEY (`nome`),
  UNIQUE KEY `nome` (`nome`),
  UNIQUE KEY `especie` (`especie`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `flor`
--

LOCK TABLES `flor` WRITE;
/*!40000 ALTER TABLE `flor` DISABLE KEYS */;
INSERT INTO `flor` VALUES ('copo-de-leite','zantedeschia aethiopica','descrição copo-de-leite',0,0,0,1,1,1,1,0,0,0,1,0),('girassol','helianthus annuus','descrição girassol',1,0,0,1,1,0,0,0,1,0,0,0),('lírio','lilium','descrição lírio',0,0,0,0,0,0,0,0,1,1,1,0),('orquídea','orquidae','descrição orquídea',1,0,1,0,1,0,0,1,1,0,0,0),('rosa','rosaceae','descrição rosa',0,1,1,0,0,0,0,0,0,0,0,0),('tulipa','tulipa','descrição tulipa',1,0,0,0,0,0,0,0,0,0,0,1);
/*!40000 ALTER TABLE `flor` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-01-23 10:34:17
